#!/usr/bin/env python3

from os import path
from setuptools import setup

here = path.abspath(path.dirname(__file__))
with open(path.join(here, 'README.rst')) as f:
        long_description = f.read()

setup(
    name='lxp',
    version='0.1.0',
    description='A new, user-friendly Python wrapper for the LXD API.',
    long_description=long_description,
    url='https://gitlab.com/agausmann/python-lxp',
    author='Adam Gausmann',
    author_email='agausmann@fastmail.com',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: Apache Software License',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.6'
    ],
    packages=[
        'lxd'
    ],
    install_requires=[
        'requests'
    ]
)
